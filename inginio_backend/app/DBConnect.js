const MongoClient = require('mongodb').MongoClient;

const DBConnect = {


 connecting: async () => {
  return new Promise((resolve,reject) => {
    MongoClient.connect(process.env.MONGOURL, {useUnifiedTopology: true},function(err, db){
      if(err) { 
        // console.err(err); 
        return reject(err);
      }else{       
        console.log('Database connected now');      
        DBConnect.dbo = db.db(process.env.MONGODB);
        return resolve(DBConnect.dbo);
      }        
    });
  });

  },

  dbo: null //=== intialized once connecting is called
}

module.exports = DBConnect;