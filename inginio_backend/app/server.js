var createError = require('http-errors');
var cors = require('cors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const { parse } = require('querystring');
const https = require('https');
const http = require('http');
var app = express();
app.use(cors({
  exposedHeaders: ['C-R']
}));


var dotenv = require('dotenv');
dotenv.config(); //Set Path here for API
/* 
dotenv.config({ path: 'path to file/.env' });
*/

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var DBConnect = require('./DBConnect');

DBConnect.connecting().then(dbo => console.log('done connecting database')).catch(err => console.log(err));

var apiRouter = require('./routes/api');
app.use('/api', apiRouter());

app.listen(3000);
module.exports = app;