const BaseModel = require('./BaseModel');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const _ = require('lodash');
const moment = require('moment');

class SampleModel extends BaseModel {

    constructor() {
        super('sample');
    }
    getNextId(callback) {
        this.getCollection(function (error, collection) {
            if (error) callback(error)
            else {
                collection.find().sort({
                    docid: -1
                }).limit(1).toArray(function (error, results) {
                    if (error) callback(error, false);
                    else callback(null, (results[0] === undefined ? 0 : results[0].docid) + 1);
                });
            }
        });
    }
    register(data, callback) {
        const __self = this;
        this.getNextUserId((err, uid) => {

            if (uid) {
                super.insert({
                    ...data
                }, (err, results) => {
                    if (_.get(results, 'length', 0) === 0) {
                        callback({
                            success: false,
                            error: 'Already registered with us.'
                        });
                    } else {
                        callback(null, {
                            success: true,
                            docid: uid
                        });
                    }
                });
            } else {
                callback({
                    success: false,
                    error: 'Create'
                });
            }
        })
    }

    getData(data, callback) {
        const query = {
            ...data
        }
        var oSelf = this;
        this.getCollection(function (err, coll) {
            var List = coll.aggregate(oSelf.getQuery(query)).toArray(function (error, results) {
                if (error) callback(error)
                else callback(null, results)
            });
        })
    }
    getQuery(cond) {

        let query = [];
        let select = {
            docid: 1,
            "seconddata.docid": 1,
        }
        query.push({
            $match: {
                ...cond
            }
        });
        query.push({
            $project: select
        });

        //Lookup Sample
        query.push({
            $lookup:{
              from: "secondcollection",
              localField: "docid",
              foreignField: "docid",          
              as: "seconddata"
            }
          });

        return query;
    }
}

module.exports = SampleModel;