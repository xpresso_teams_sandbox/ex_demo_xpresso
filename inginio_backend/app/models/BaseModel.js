const DBOConnect = require('./../DBConnect');
const _ = require('lodash');

class BaseModel{

  constructor(table){    
    this.table = table;
    this.dbo = null;
    //this.connect();
  }

  connect(callback){
    var oSelf = this;
    if(DBOConnect.dbo){
      callback(null,DBOConnect.dbo);
    }else{
      callback('failed db connect',null);
    }
  }

  getCollection(callback) {
    var oSelf = this;
    this.connect(function(err,dbo){
      try{        
        var col = dbo.collection(oSelf.table);        
        callback(null,col);
      }catch(e){
        callback(e,null);
      }
    });
  }

  find(query={},callback) {
    this.getCollection(function(error, collection) {
      if( error ) callback(error)
      else {        
        collection.find(query).toArray(callback);
      }
    });
  }

  set(whereData,updateData,callback,upsert = true){        
    this.getCollection(function(err,coll){
      coll.update(whereData,updateData,{upsert: upsert}, function(err,res){
        if( err ) callback(err);
        else{          
          callback(null, res.result.nModified);
        } 
      });
    });
  }

  insert(data,callback){
    this.getCollection(function(err,coll){
      coll.insert(data, (err,results) => {        
        //console.log('Inserted==========',err,results);
        if(err) callback(err,null);
        else callback(null,results.ops);
      });
    })
  }

  update(query,set,callback){
    this.getCollection(function(err,coll){
      coll.update(query,{$set: set}, callback);
    })
  }
  remove(query,callback){
    this.getCollection(function(err,coll){
      coll.deleteOne(query, callback);
    })
  }

}

module.exports = BaseModel;