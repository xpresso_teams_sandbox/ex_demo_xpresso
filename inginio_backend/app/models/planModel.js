const BaseModel = require('./BaseModel');
const _ = require('lodash');

class planModel extends BaseModel {

    constructor() {
        super('plandocs');
    }  
    getData(data, callback) {
        let query = {
            ...data
        }
        var oSelf = this;
        this.getCollection(function (err, coll) {
            var List = coll.aggregate(oSelf.getQuery(query)).toArray(function (error, results) {
                if (error) callback(error)
                else callback(null, results)
            });
        })
    }
    getQuery(cond) {
        let query = [];
        let select = {
           _id:0            
        }
        if (!_.isEmpty(_.get(cond, 'plan_id', ""))) {
            let planQuery = {
                "plan_id": cond.plan_id
            }
            query.push({
                $match: {
                    ...planQuery
                }
            });
        }
        
        if (!_.isEmpty(_.get(cond, 'ndc', ""))) {
            select = {
                _id:0,
                plan_id:1,
                plan_name:1,
                drugs:1
            }
            let ndcQuery = {}
            ndcQuery["plans.plan_id"] = cond["plan_id"]
            ndcQuery["ndc"] = cond["ndc"]            
            query.push({
                $lookup: {
                    from: "druglists",
                    pipeline: [{
                            $match: {
                                ...ndcQuery
                            }
                        },
                        {
                            $project: {
                                drugname: 1,
                                ndc: 1,
                                _id: 0,
                                "plans": {
                                    $filter: {
                                        input: "$plans",
                                        as: "plans",
                                        cond: {
                                            $eq: ["$$plans.plan_id", cond["plan_id"]]
                                        }
                                    }
                                }
                            }
                        }
                    ],
                    as: "drugs"
                }
            });
        }
        query.push({
            $project: select
        });
        return query;
    }
}

module.exports = planModel;