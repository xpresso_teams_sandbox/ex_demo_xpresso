const BaseModel = require('./BaseModel');
const _ = require('lodash');

const filterResult = (result,callback)=>{
    
    if(result[0]["planDocs"]){
        let newData = []; newData[0] = {}
        newData[0]["drugname"] = result[0]["drugname"];
        newData[0]["ndc"] = result[0]["ndc"];
        newData[0]["planDetails"] = result[0]["plans"][0];
    for(var i = 0;i<result.length; i++){
        var selectedTier = result[i]["plans"][0]["tier"];
        var PlanDocs = result[i]["planDocs"];
        var SelectedPlan = {}
        SelectedPlan["plan_name"] = PlanDocs[0]["plan_name"]
        PlanDocs[0]["tiers"].map((i,k) =>{            
            if (i["tier"] === parseInt(selectedTier)){
                SelectedPlan["Plan_Tier"] = i
            }            
        })
        newData[i]["plan_name"] = SelectedPlan["plan_name"];        
        newData[i]["planTier"] = SelectedPlan["Plan_Tier"];        
    }
        callback(null, newData)
    }
    else{
        callback(null, result)
    }
}
class DrugsModel extends BaseModel {
    constructor() {
        super('druglists');
    }

    update(data,callback){
      var filter = {ndc: data.ndc};
      let  updateData = {};
      if(!_.isEmpty(_.get(data,'option').toString(),"")){              
        if(data.option === "info"){
            updateData = {$set:data.details}
        }
        else if(data.option === "addplan"){
            updateData = {$push:{plans:data.details}}
        }
        else if(data.option === "removeplan"){
            updateData = {$pull:{plans:data.details}}
        }
      } 
      
        this.getCollection(function (err, coll) {
            coll.findOneAndUpdate(filter,updateData  , {new:true}, function(err, doc) {
                if(err){ callback(err,null);}
                else{
                  let updatedData = {};
                  for(var key in updateData){
                    updatedData[key] = updateData[key]
                  }
                  callback(null,updatedData)
                }
              });
        })
    }

    getList(data,callback){       
        var oSelf = this;
        this.getCollection(function (err, coll) {
            var List = coll.aggregate(oSelf.getQuery(data)).toArray(function (error, results) {
                if (error) callback(error)
                else filterResult(results,callback)
            });
        })
    }
    getQuery(params){
        let query = {
        };
        if(_.get(params,'ndc') !== undefined){
            if(!_.isEmpty(_.get(params,'ndc').toString(),"")){              
                query["ndc"] = params.ndc;
            }            
        }
        if(!_.isEmpty(_.get(params,'drugname',""))){           
              query["drugname"]= {"$regex": decodeURI(params.drugname), "$options": "i"};            
        }
        let select = {
            "ndc": 1,
            "drugname": 1,
            "plans": 1,
            "_id":0
        }
        let querys = [];
        querys.push({
            $match: {
                ...query
            }
        });
        if(!_.isEmpty(_.get(params,'plan_id',""))){  
            select = {
                "ndc": 1,
                "drugname": 1,
                "plans": {
                    $filter: {
                        input: "$plans",
                        as: "plans",
                        cond: {
                            $eq: ["$$plans.plan_id", params["plan_id"]]
                        }
                    }
                },
                
                "planDocs":1,
                "_id":0
            } 
            let planQuery = {}
            planQuery["plan_id"] = params["plan_id"]
            querys.push({
                $lookup: {
                    from: "plandocs",
                    pipeline: [{
                            $match: {
                                ...planQuery
                            }
                        },
                        {
                            $project: {                               
                                _id: 0                                
                            }
                        }
                    ],
                    as: "planDocs"
                }
            });
        }
        querys.push({
            $project: select
        });
        return querys;
    }
}
module.exports = DrugsModel;