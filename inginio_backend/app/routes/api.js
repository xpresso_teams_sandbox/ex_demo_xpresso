const express = require('express');
const router = express.Router();

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const _ = require('lodash');
const planModel = require('./../models/planModel');
const DrugsModel = require('./../models/DrugsModel');

const verifyToken = (req) => {
    try{
      let authHeader = req.header('Authorization');    
  
      if(authHeader && !_.isNull(authHeader) && !_.isEmpty(authHeader)){ 
        
        let sessionID = authHeader.split(' ')[1];
       
        
        if (sessionID === process.env.AUTHTOKEN) {
          return true;
        }
      }
    }catch(e){
      return false;
    }
    return false;
  };

  module.exports = function() {
    
    
    router.post('/getPlans',function(req,res,next){  
      const session = verifyToken(req)      
        if(session){                   
          const Pt = new planModel();
          const data = req.body;
          Pt.getData({...data},function(err,success){        
            if(err){ 
              return res.json({success: false, msg: "Service error"});
            }else{
              res.json(success);
            }
          });
        }
        else{
          return res.json({success: false, msg: "Unauthorized Access"});
        }
        
    });
    router.post('/getDrugs',function(req,res,next){  
      const session = verifyToken(req)      
        if(session){                   
          const drgs = new DrugsModel();
          const data = req.body;
          drgs.getList({...data},function(err,success){        
            if(err){     
              return res.json({success: false, msg: "Service error"});
            }else{
              res.json(success);
            }
          });
        }
        else{
          return res.json({success: false, msg: "Unauthorized Access"});
        }
        
    });
    router.post('/updateDrug',function(req,res,next){
      const session = verifyToken(req)      
      if(session){ 
        const drgs = new DrugsModel();
          const data = req.body;
          drgs.update({...data},function(err,success){        
            if(err){     
              return res.json({success: false, msg: "Service error"});
            }else{
              res.json(success);
            }
          });
      }
      else{
        return res.json({success: false, msg: "Unauthorized Access"});
      }
    }
    );
    return router;
}