#! /bin/bash
## This script is used to run the project. It should contain the script which will run the project
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user
set -e

# Run the application
cd ${ROOT_FOLDER}/${PROJECT_NAME}
node server.js
