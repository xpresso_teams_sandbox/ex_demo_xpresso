# Paths variable
MOUNT_PATH = "/data"
PDF_FOLDER = "content"
PLANS_FOLDER = "processed"
TIERS_FOLDER = "tiers"

TIERS_FILENAME = "tiers.json"

# Mongo Key to send results
MONGO_KEY = "mongodb://172.16.2.73:27017/test?w=majority"
