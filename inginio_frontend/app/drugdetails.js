//  ******************************************* Window Onload function ************************************************* 
let populatePlanDropdown =function(){
	let getPlans=  JSON.parse(localStorage.getItem('planDetails'));
	console.log(getPlans);
	$.each(getPlans[0].plans, function(i, item) {
      $("#selectPlan").append($("<option>").attr('value', item.plan_id).text(item.plan_id + "-" + item.plan_name));
	  
    });
};

//  ******************************************* On select trigger get tier and cost share elements******************************************************
/*
let getTierValue =function(){
	console.log("Inside Tier value");
	let planId=document.getElementById("selectPlan").value;
	let ndc = document.getElementById("drugCode").value;
	let tier = '';
    $.post({
        statusCode: {
            200: function(data){
				console.log(data);
                localStorage.setItem('planDetails',JSON.stringify(data));
				data.forEach(function(planObj)
				{
					if (planObj.plan_id == planId){
						planObj.drugs.forEach(
							function(drug){
								if (drug.ndc == ndc) 
									displayCostDetails(drug.tier);
									break;}) 
						break;
					}
				}
					)
                }
            },
		data: {'ndc': `'${document.getElementById("drugCode").value}'`,'plan_id':`'${planId}'`},
        url: `${localStorage.getItem('service location')}api/getplans`,
		beforeSend: function (xhr) {
			xhr.setRequestHeader("Authorization", "Bearer " + "5654c1fa4cf5431c1ee67ccbf1d1a87c");
			xhr.setRequestHeader("withCredentials",true);
		}
    });
};
*/
let displayCostDetails =function(){	
	let planId=document.getElementById("selectPlanTxtBox").value;  
	let ndc = document.getElementById("autoCompleteNdc").value;
	console.log(planId);
	console.log(ndc);
    $.post({
        statusCode: {
            200: function(data){
				console.log(data);
				console.log("drugname "+data[0].drugname);
				let quantity="", days="", tier="", pa="",bvsd="",st="";
				let retail_90day="",retail_30day="";
				let mailorder_90day="", mailorder_30day="";
				let paymentModifier="";
				data.forEach(function(plan){
					tier=plan.planDetails.tier;
					quantity=plan.planDetails.ql.qnt;
					console.log("Quantity is "+quantity);
					days=plan.planDetails.ql.days;
					if (plan.planDetails.st==true) st="Yes"; else st="No";
					if (plan.planDetails.pa==true) pa="Yes"; else pa="No";
					if (plan.planDetails.bvsd==true) bvsd="Yes"; else bvsd="No";
					console.log(bvsd);
					if(plan.planTier.retail["30-day"].search(/[^\$]/g)) paymentModifier="Copay"; else paymentModifier="Coinsurance";
					retail_30day= plan.planTier.retail["30-day"];
					retail_90day= plan.planTier.retail["90-day"];
					mailorder_30day= plan.planTier.mailorder["30-day"];
					mailorder_90day= plan.planTier.mailorder["90-day"];
					//retail_90day_preferred= plan.planTier.mailorder;
				}	
				);
				console.log(document.getElementById(cost));
				document.getElementById("cost").innerHTML=`<tr><th colspan='3'>Drug Name: ${data[0].drugname} </th></tr>
									    <tr>
											<td colspan='2'>Quantity Limit</td> 
											<td>${quantity}/${days} days</td>
										</tr>
										<tr>
											<td colspan='2'>Tier</td>
											<td>${tier}</td>
										</tr>
										<tr>
											<td colspan='2'>Prior Authorization Required</td>
											<td>${pa}</td>
										</tr>
										<tr>
											<td colspan='2'>Step Therapy Required</td>
											<td>${st}</td>
										</tr>
										<tr>
											<td colspan='2'>B vs D</td>
											<td>${bvsd}</td>
										<tr><th colspan='3'>Costshare</th></tr>
										<tr>
											<td>Delivery Medium</td>
											<td>Retail</td>
											<td>Mail Order</td>
										</tr>
										<tr>
											<td>30-Day (${paymentModifier})</td>
											<td>${retail_30day}</td>
											<td>${mailorder_30day}</td>
										</tr>
										<tr>
											<td>90-Day (${paymentModifier})</td>
											<td>${retail_90day}</td>
											<td>${mailorder_90day}</td>
										</tr>`;			
				document.getElementById('cost').style.display = 'block';
            }
		},
		data: {'ndc':`${ndc}`,'plan_id':`${planId}`},
		url: `${localStorage.getItem('service location')}api/getDrugs`,
		beforeSend: function (xhr) {
			xhr.setRequestHeader("Authorization", "Bearer " + "5654c1fa4cf5431c1ee67ccbf1d1a87c");
			xhr.setRequestHeader("withCredentials",true);
		}
    });
};

//  ******************************************* Window Onload function ************************************************* 

window.onload = function(){
    let serviceLocation = `http://64.74.216.162:30910`;
    if(serviceLocation[serviceLocation.length-1] !== '/'){
        serviceLocation += '/';
    }
	//document.getElementById('drugPlan').style.display = 'none';
	//document.getElementById('selectPlan').style.display = 'none';
	localStorage.setItem('service location', serviceLocation);
	$('#planSearch').click(e => {
        e.preventDefault();
		console.log(document.getElementById("autoCompleteNdc").value); 
		let ndc = document.getElementById("autoCompleteNdc").value;
        $.post({
            statusCode: {
                200: function(data){
                    localStorage.setItem('planDetails',JSON.stringify(data));
					$("#selectPlan").show();
					$("#selectPlanTxtBox").show();
					$("#drugPlan").show();
					populatePlanDropdown()
                }
            },
			data: {'ndc': `${ndc}`},
            url: `${localStorage.getItem('service location')}api/getDrugs`,
			beforeSend: function (xhr) {
				xhr.setRequestHeader("Authorization", "Bearer " + "5654c1fa4cf5431c1ee67ccbf1d1a87c");
				xhr.setRequestHeader("withCredentials",true);
			}
        });
    });
	$('#selectPlan').change(displayCostDetails);
	document.querySelector('#uploadPlan').onclick=function(e){
		console.log("hello");
		window.location.href= "./upload.html";
	};
	
	$("#selectPlanTxtBox").on('input', function () {
		var val = this.value;
		if($('#selectPlan option').filter(function(){
			return this.value.toUpperCase() === val.toUpperCase();        
		}).length) {	
			displayCostDetails();
		}
});
};
//  ******************************************* Autocomplete Drug search ************************************************* 

	

