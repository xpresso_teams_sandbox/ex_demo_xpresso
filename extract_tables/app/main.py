"""
This is the implementation of data preparation for sklearn
"""

import json
import logging
import os
import re
import sys
from pprint import pprint

import camelot
import pandas as pd
import tabula
from config import rx_config
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

logger = XprLogger("extract_tables", level=logging.INFO)


class ExtractTables(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="ExtractTables")
        """ Initialize all the required constansts and data her """
        self.mount_path = rx_config.MOUNT_PATH
        self.processed_path = os.path.join(self.mount_path, rx_config.PLANS_FOLDER)
        self.tiers_path = os.path.join(self.mount_path, rx_config.TIERS_FOLDER)
        self.insurer_plans = pd.read_csv(os.path.join(self.processed_path, 'insurer_plans.csv'))
        self.tiers = None

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)

            tables = self.extract_tables(self.insurer_plans['insurer'][0],
                                         self.insurer_plans['filepath'][0])

            standard_cost_share_table = self.select_tables(self.insurer_plans['insurer'][0], tables)

            cost_dict = self.row_cols(standard_cost_share_table, False)

            cost_dict_wo_nan, blanks = self.find_blank_cells(self.insurer_plans['insurer'][0],
                                                             cost_dict, print=False)

            tier_rows = self.get_tier_rows(cost_dict_wo_nan, blanks)
            # Finding tiers and storing it as a dictionary
            self.tiers = self.get_tierwise_cost_share(
                self.insurer_plans['insurer'][0], cost_dict_wo_nan, tier_rows)

            # Saving tiers
            if not os.path.exists(self.tiers_path):
                os.mkdir(self.tiers_path)
            with open(os.path.join(self.tiers_path, 'tiers.json'), 'w') as fout:
                json.dump(self.tiers, fout)
            fout.close()
            # Saving to csv file
            self.insurer_plans.to_csv(os.path.join(self.processed_path, "insurer_plans.csv"),
                                      index=False)
        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()

    @staticmethod
    def matcher(x, entities):
        for i in entities:
            if i in x:
                return x
            else:
                pass

    def get_tierwise_cost_share(self, insurer, cost_dict, tier_rows):
        tiers = []
        for row in tier_rows:
            tier = self.create_tier_structure()
            row_dict = cost_dict[row]
            columns = row_dict.keys()
            if insurer == rx_config.INSURERS[1]:
                for item in columns:
                    item_split = row_dict[item].split('\n')
                    try:
                        tier_name = list(
                            filter(lambda x: self.matcher(x, rx_config.TIER_ENTITIES), item_split))[
                            0]
                        tier_forms = {"Preferred Generic": 1, "Generic": 2, "Preferred Brand": 3,
                                      "Non-Preferred": 4, "Specialty": 5}
                        keys = list(tier_forms.keys())
                        for i in range(len(keys)):
                            if re.findall(keys[i], tier_name):
                                if tier['tier'] == '':
                                    tier['tier'] = tier_forms[keys[i]]
                                else:
                                    continue
                    except IndexError:
                        copay = list(filter(lambda x: self.matcher(x, ["$"]), item_split))
                        coinsurance = list(
                            filter(lambda x: self.matcher(x, ["%", "N/A"]), item_split))
                        if copay:
                            if item == 1:
                                tier['retail']['30-day'] = copay[0]
                                tier['retail']['90-day'] = copay[1]
                            elif item == 2:
                                tier['mailorder']['30-day'] = copay[0]
                                tier['mailorder']['90-day'] = copay[1]
                        if coinsurance:
                            if item == 1:
                                tier['retail']['30-day'] = coinsurance[0]
                                tier['retail']['90-day'] = coinsurance[1]
                            elif item == 2:
                                tier['mailorder']['30-day'] = coinsurance[0]
                                tier['mailorder']['90-day'] = coinsurance[1]
                tiers.append(tier.copy())

            if insurer == rx_config.INSURERS[0]:
                for item in columns:
                    item_split = row_dict[item].split("\n")
                    try:
                        tier_name = list(
                            filter(lambda x: self.matcher(x, rx_config.TIER_ENTITIES), item_split))[
                            0]
                        tier_forms = {"Generic": 1, "Preferred": 2, "Nonpreferred": 3,
                                      "Specialty": 4}
                        keys = list(tier_forms.keys())
                        for i in range(len(keys)):
                            if re.findall(keys[i], tier_name):
                                tier['tier'] = tier_forms[keys[i]]
                    except IndexError:
                        copay = list(filter(lambda x: self.matcher(x, ["$"]), item_split))
                        coinsurance = list(
                            filter(lambda x: self.matcher(x, ["%", "N/A"]), item_split))
                        if copay:
                            if self.matcher(item.lower(), rx_config.RETAIL_TERMS):
                                if len(copay) == 1:
                                    tier['retail']['30-day'] = copay[0]
                                    tier['retail']['90-day'] = 'NA'
                            if self.matcher(item.lower(), rx_config.DELIVERY_TERMS):
                                if len(copay) == 1:
                                    tier['mailorder']['30-day'] = copay[0]
                                    tier['mailorder']['90-day'] = 'NA'

                        if coinsurance:
                            if self.matcher(item.lower(), rx_config.RETAIL_TERMS):
                                if len(copay) == 1:
                                    tier['retail']['30-day'] = coinsurance[0]
                                    tier['retail']['90-day'] = 'NA'
                            if self.matcher(item.lower(), rx_config.DELIVERY_TERMS):
                                if len(copay) == 1:
                                    tier['mailorder']['30-day'] = coinsurance[0]
                                    tier['mailorder']['90-day'] = 'NA'
                tiers.append(tier.copy())
        return tiers

    @staticmethod
    def find_blank_cells(insurer, cost_dict, print=False):
        if insurer == rx_config.INSURERS[0]:
            new_cost_dict = {}
            blank_row = []
            for k, v in cost_dict.items():
                new_cost_dict[k] = {}
                for key, value in v.items():
                    if isinstance(value, float):
                        new_cost_dict[k][key] = 'Blank'
                        blank_row.append(k)
                    else:
                        new_cost_dict[k][key] = value
            if print:
                pprint(new_cost_dict)
            return new_cost_dict, blank_row
        else:
            return cost_dict, []

    @staticmethod
    def get_tier_rows(cost_dict_wo_nan, blanks):
        all_rows = list(cost_dict_wo_nan.keys())
        tier_rows = [x for x in all_rows if x not in set(blanks)]
        return tier_rows

    @staticmethod
    def convert_list_to_dict(lst):
        res_dct = {lst[i]: "" for i in range(0, len(lst))}
        return res_dct

    def create_tier_structure(self):
        tier_structure = {}
        for field in rx_config.FIELDS:
            if field == "tier":
                tier_structure[field] = ""
            else:
                tier_structure[field] = {}
                tier_structure[field]["30-day"] = self.convert_list_to_dict(rx_config.PAYMENT_TYPES)
                tier_structure[field]["90-day"] = self.convert_list_to_dict(rx_config.PAYMENT_TYPES)
        return tier_structure

    @staticmethod
    def extract_tables(insurer, filepath):
        tables = None
        if insurer == rx_config.INSURERS[0]:  # ESI
            # tables = camelot.read_pdf(filepath, flavor='stream', kind='textedge', pages = "1-end")
            tables = tabula.read_pdf(filepath, pages="all")
        elif insurer == rx_config.INSURERS[1]:  # HUMANA
            tables = camelot.read_pdf(filepath, flavor='lattice', pages="1-end")
        return tables

    @staticmethod
    def select_tables(insurer, tables):
        standard_cost_share_table = pd.DataFrame()
        if insurer == rx_config.INSURERS[0]:  # ESI
            standard_cost_share_table = tables[0].dropna(how='all').dropna(axis='columns',
                                                                           how='all')
        elif insurer == rx_config.INSURERS[1]:  # HUMANA
            preferred_cost_share_table = tables[0].df
            standard_cost_share_table = tables[1].df
            medicare_cost_share_table = tables[2].df
        return standard_cost_share_table

    @staticmethod
    def row_cols(df, print=False):
        cost_dict = df.to_dict('index')
        if print:
            pprint(cost_dict)
        return cost_dict

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        cmd = f'cp -r {self.tiers_path} {self.OUTPUT_DIR}'
        os.system(cmd)

        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = ExtractTables()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
