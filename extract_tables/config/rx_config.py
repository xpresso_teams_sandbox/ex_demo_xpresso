# Paths variable
MOUNT_PATH = "/data"
PDF_FOLDER = "content"
PLANS_FOLDER = "processed"
TIERS_FOLDER = "tiers"

# Details of insurer and plans
INSURERS = ["Express Scripts", "Humana"]
ES_PLAN_ID_TERMS = ["PPO", "H.S.A", "P.P.O"]
HUMANA_PLAN_ID_TERMS = ["PDP"]

# Fields for cost share object
FIELDS = ["tier", "retail", "mailorder"]
PAYMENT_TYPES = ["copay", "deductible", "coinsurance"]

# Humana terms and tiers
TIER_ENTITIES = ["Tier", "Preferred Generic", "Generic", "Preferred Brand", "Non-Preffered",
                 "Specialty", "Preferred", "Nonpreferred"]
DELIVERY_TERMS = ['home delivery', 'mail order']
RETAIL_TERMS = ["retail", "pharmacy", "store"]
