"""
This is the implementation of data preparation for sklearn
"""

import logging
import os
import re
import sys

import pandas as pd
import pdfminer.high_level
from config import rx_config
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

logger = XprLogger("fetch_plans", level=logging.INFO)


class FetchPlans(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="FetchPlans")
        """ Initialize all the required constansts and data her """
        self.mount_path = rx_config.MOUNT_PATH
        self.pdf_path = os.path.join(self.mount_path, rx_config.PDF_FOLDER)
        self.file_name = sys.argv[2]

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)

            if not self.file_name:
                raise Exception('No file name provided')

            print("Plan Folder", self.pdf_path, "\n")
            if not os.listdir(self.pdf_path):
                raise Exception(f"No plans to extract information at {self.pdf_path}")

            file_path = os.path.join(self.pdf_path, self.file_name)
            insurer, plan_id, plan_name = self.get_plan_id(file_path)

            insurer_plans = pd.DataFrame(data=[[insurer, plan_id, plan_name, file_path]],
                                         columns=['insurer', 'plan_id', 'plan_name', 'filepath'])
            print(f"insurer: {insurer},\nplan_id: {plan_id},\nplan_name: {plan_name}\n")
            if not os.path.exists(os.path.join(self.mount_path, 'processed')):
                print("Making new directory", os.path.join(self.mount_path, 'processed'))
                os.mkdir(os.path.join(self.mount_path, 'processed'))
            insurer_plans.to_csv(os.path.join(self.mount_path, 'processed', "insurer_plans.csv"),
                                 index=False)
        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()

    @staticmethod
    def matcher(x, entities):
        for i in entities:
            if i in x:
                return x
            else:
                pass

    def get_plan_id(self, filepath):
        try:
            '''
            - Extract first page from PDF to find Plan ID and Plan Name.
            - Filter Blank lines, Look for Plan Terms 
            '''
            pdf_extract = pdfminer.high_level.extract_text(filepath, page_numbers=[0])
            check_plan = re.findall("|".join(rx_config.INSURERS), pdf_extract)
            insurer = check_plan[0]
            split = pdf_extract.split("\n")
            split_non_blanks = [i.strip() for i in split if i.strip() != '']
            if insurer == rx_config.INSURERS[0]:
                plan_id = \
                    list(filter(lambda x: self.matcher(x, rx_config.ES_PLAN_ID_TERMS),
                                split_non_blanks))[0]
                plan_name = plan_id
            elif insurer == rx_config.INSURERS[1]:
                plan_id = \
                    list(filter(lambda x: self.matcher(x, rx_config.HUMANA_PLAN_ID_TERMS),
                                split_non_blanks))[0]
                plan_name_pat = '^' + insurer + r'.* Plan$'
                plan_name = [re.findall(plan_name_pat, x)[0] for x in split_non_blanks if
                             re.findall(plan_name_pat, x) != []][0]
            else:
                insurer = 'Insurer not found'
                plan_id = "No plan found"
                plan_name = "No plan found"
        except FileNotFoundError:
            insurer = 'Insurer not found'
            plan_id = "No plan found"
            plan_name = "No plan found"
        return insurer, plan_id, plan_name

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "fetch_plans"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = FetchPlans()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
